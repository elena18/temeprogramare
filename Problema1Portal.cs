using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problema1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Introduceti un numar:");
            int n = int.Parse(Console.ReadLine());
            int[] vector = new int[n];
            Console.WriteLine("Introduceti elementele vectorului");
            for(int i=0;i<vector.Length;i++)
            {
                vector[i] = int.Parse(Console.ReadLine());
            }
            Console.WriteLine("Introduceti 2 valori pentru a si b");
            int a = int.Parse(Console.ReadLine());
            int b = int.Parse(Console.ReadLine());
            int numar = 0, suma = 0;
            for (int i = 0; i < vector.Length; i++)
            {
                if (vector[i] >= a && vector[i] <= b)
                {
                    suma += vector[i];
                    numar++;
                }

            }
            float media = (float)suma / numar;
            Console.WriteLine("Media este "+ media);
            Console.ReadKey();

        }
    }
}