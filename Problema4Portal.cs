using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problema4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Introduceti un numar:");

            int n = int.Parse(Console.ReadLine());
            int[] vector = new int[n];
            Console.WriteLine("Introduceti elementele vectorului");
            for (int i = 0; i < vector.Length; i++)
            {
                vector[i] = int.Parse(Console.ReadLine());
            }
            for (int i = 0; i < vector.Length ; i += 2)
            {
                Array.Resize(ref vector, vector.Length + 1);
                int j;
                for (j = vector.Length - 1; j > i + 1; j--)
                {
                    vector[j] = vector[j - 1];
                }
                vector[i + 1] = vector[i] + vector[i + 1];

            }
            for (int i = 0; i < vector.Length; i++)
            {
                Console.Write(" " + vector[i]);
            }

            Console.ReadKey();

        }
    }
}