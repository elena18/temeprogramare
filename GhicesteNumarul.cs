using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema_seminar5
{
    class Program
    {
        static void Main(string[] args)
        {
            Random solutie = new Random();
            int nrRandom = solutie.Next(1, 100);
            int numarGhicit;
            int nr_introdus = 0;
            do
            {
                Console.WriteLine("Ghiceste numarul!");
                numarGhicit = int.Parse(Console.ReadLine());
                nr_introdus++;
                if (nrRandom == numarGhicit)
                {
                    Console.WriteLine("Felicitari, ati ghicit numarul, iar numarul de incasari este " + nr_introdus);

                    Console.ReadKey();
                    return;
                }
                if (Math.Abs(nrRandom - numarGhicit) <= 3)
                {
                    Console.WriteLine("Foarte fierbinte, mai incearca!");
                }
                else if (Math.Abs(nrRandom - numarGhicit) <= 5)
                {
                    Console.WriteLine("Fierbinte, mai incearca!");
                }
                else if (Math.Abs(nrRandom - numarGhicit) <= 10)
                {
                    Console.WriteLine("Cald, mai incearca!");
                }
                else if (Math.Abs(nrRandom - numarGhicit) <= 20)
                {
                    Console.WriteLine("Caldut, mai incearca!");
                }
                else
                {
                    Console.WriteLine("Rece, mai incearca!");
                }

            } while (nrRandom != numarGhicit);
            Console.ReadKey();
        }
    }
}
