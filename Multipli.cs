using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema_multipli
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Introduceti cele doua numere:");
            int x = int.Parse(Console.ReadLine());
            int y = int.Parse(Console.ReadLine());
            Console.WriteLine($"Suma multiplilor lui x mai mici decat y este {SumaMultipli(x, y)}");
            Console.ReadKey();
        }
        static int SumaMultipli(int x, int y)
        {
            int suma = 0, multiplu = x;
            while (multiplu < y)
            {
                suma += multiplu;
                multiplu += x;
            }
            return suma;
        }
    }
}
